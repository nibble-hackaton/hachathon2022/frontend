import { createRouter, createWebHashHistory } from 'vue-router'
import MainView from "@/views/MainView";
import ProfileView from "@/views/ProfileView";
import RegisterView from "@/views/RegisterView";
import AuthView from "@/views/AuthView";

const routes = [
  {
    path: '/',
    name: 'home',
    component: MainView
  },
  {
    path: '/profile',
    name: 'profile',
    component: ProfileView
  },
  {
    path: '/register',
    name: 'register',
    component: RegisterView
  },
  {
    path: '/login',
    name: 'login',
    component: AuthView
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
