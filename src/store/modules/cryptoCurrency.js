export default {
    state: {
        cryptoCurrency: []
    },
    getters: {
        getCryptoCurrency(state) {
            return state.cryptoCurrency
        }
    },
    mutations: {
    },
    actions: {
    }
}